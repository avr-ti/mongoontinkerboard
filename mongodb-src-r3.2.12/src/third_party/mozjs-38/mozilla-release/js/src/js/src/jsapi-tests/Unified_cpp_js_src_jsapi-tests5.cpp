#define MOZ_UNIFIED_BUILD
#include "testUTF8.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testUTF8.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testUTF8.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testUbiNode.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testUbiNode.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testUbiNode.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testUncaughtError.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testUncaughtError.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testUncaughtError.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testWeakMap.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testWeakMap.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testWeakMap.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testXDR.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testXDR.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testXDR.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "tests.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "tests.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "tests.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif