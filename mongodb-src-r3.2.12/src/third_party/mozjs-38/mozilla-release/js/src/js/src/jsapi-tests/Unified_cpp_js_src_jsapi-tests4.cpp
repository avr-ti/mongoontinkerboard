#define MOZ_UNIFIED_BUILD
#include "testProfileStrings.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testProfileStrings.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testProfileStrings.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testPropCache.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testPropCache.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testPropCache.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testRegExp.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testRegExp.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testRegExp.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testResolveRecursion.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testResolveRecursion.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testResolveRecursion.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testSameValue.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testSameValue.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testSameValue.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testSavedStacks.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testSavedStacks.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testSavedStacks.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testScriptInfo.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testScriptInfo.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testScriptInfo.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testScriptObject.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testScriptObject.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testScriptObject.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testSetProperty.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testSetProperty.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testSetProperty.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testSetPropertyIgnoringNamedGetter.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testSetPropertyIgnoringNamedGetter.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testSetPropertyIgnoringNamedGetter.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testSourcePolicy.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testSourcePolicy.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testSourcePolicy.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testStringBuffer.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testStringBuffer.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testStringBuffer.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testStructuredClone.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testStructuredClone.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testStructuredClone.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testSymbol.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testSymbol.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testSymbol.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testToIntWidth.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testToIntWidth.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testToIntWidth.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testTypedArrays.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testTypedArrays.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testTypedArrays.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif