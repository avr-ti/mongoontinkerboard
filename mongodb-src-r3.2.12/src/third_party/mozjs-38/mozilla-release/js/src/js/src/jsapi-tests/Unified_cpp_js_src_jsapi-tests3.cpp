#define MOZ_UNIFIED_BUILD
#include "testJitMoveEmitterCycles-mips.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testJitMoveEmitterCycles-mips.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testJitMoveEmitterCycles-mips.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testJitMoveEmitterCycles.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testJitMoveEmitterCycles.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testJitMoveEmitterCycles.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testJitRValueAlloc.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testJitRValueAlloc.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testJitRValueAlloc.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testJitRangeAnalysis.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testJitRangeAnalysis.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testJitRangeAnalysis.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testLookup.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testLookup.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testLookup.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testLooselyEqual.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testLooselyEqual.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testLooselyEqual.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testMappedArrayBuffer.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testMappedArrayBuffer.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testMappedArrayBuffer.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testMutedErrors.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testMutedErrors.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testMutedErrors.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testNewObject.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testNewObject.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testNewObject.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testNullRoot.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testNullRoot.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testNullRoot.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testOOM.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testOOM.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testOOM.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testObjectEmulatingUndefined.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testObjectEmulatingUndefined.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testObjectEmulatingUndefined.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testOps.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testOps.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testOps.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testParseJSON.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testParseJSON.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testParseJSON.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testPersistentRooted.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testPersistentRooted.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testPersistentRooted.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testPreserveJitCode.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testPreserveJitCode.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testPreserveJitCode.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif