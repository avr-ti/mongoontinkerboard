#define MOZ_UNIFIED_BUILD
#include "testDefinePropertyIgnoredAttributes.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testDefinePropertyIgnoredAttributes.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testDefinePropertyIgnoredAttributes.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testEnclosingFunction.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testEnclosingFunction.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testEnclosingFunction.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testErrorCopying.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testErrorCopying.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testErrorCopying.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testException.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testException.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testException.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testExternalStrings.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testExternalStrings.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testExternalStrings.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testFindSCCs.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testFindSCCs.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testFindSCCs.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testForOfIterator.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testForOfIterator.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testForOfIterator.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testForwardSetProperty.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testForwardSetProperty.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testForwardSetProperty.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testFreshGlobalEvalRedefinition.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testFreshGlobalEvalRedefinition.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testFreshGlobalEvalRedefinition.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testFunctionProperties.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testFunctionProperties.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testFunctionProperties.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testGCAllocator.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testGCAllocator.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testGCAllocator.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testGCCellPtr.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testGCCellPtr.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testGCCellPtr.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testGCChunkPool.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testGCChunkPool.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testGCChunkPool.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testGCExactRooting.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testGCExactRooting.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testGCExactRooting.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testGCFinalizeCallback.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testGCFinalizeCallback.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testGCFinalizeCallback.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif
#include "testGCHeapPostBarriers.cpp"
#ifdef PL_ARENA_CONST_ALIGN_MASK
#error "testGCHeapPostBarriers.cpp uses PL_ARENA_CONST_ALIGN_MASK, so it cannot be built in unified mode."
#undef PL_ARENA_CONST_ALIGN_MASK
#endif
#ifdef INITGUID
#error "testGCHeapPostBarriers.cpp defines INITGUID, so it cannot be built in unified mode."
#undef INITGUID
#endif