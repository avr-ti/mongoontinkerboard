# THIS FILE WAS AUTOMATICALLY GENERATED. DO NOT EDIT.

CPPSRCS += TestArrayUtils.cpp
CPPSRCS += TestAtomics.cpp
CPPSRCS += TestBinarySearch.cpp
CPPSRCS += TestBloomFilter.cpp
CPPSRCS += TestCasting.cpp
CPPSRCS += TestCeilingFloor.cpp
CPPSRCS += TestCheckedInt.cpp
CPPSRCS += TestCountPopulation.cpp
CPPSRCS += TestCountZeroes.cpp
CPPSRCS += TestEndian.cpp
CPPSRCS += TestEnumSet.cpp
CPPSRCS += TestFloatingPoint.cpp
CPPSRCS += TestIntegerPrintfMacros.cpp
CPPSRCS += TestJSONWriter.cpp
CPPSRCS += TestMacroArgs.cpp
CPPSRCS += TestMacroForEach.cpp
CPPSRCS += TestMaybe.cpp
CPPSRCS += TestPair.cpp
CPPSRCS += TestPoisonArea.cpp
CPPSRCS += TestRefPtr.cpp
CPPSRCS += TestRollingMean.cpp
CPPSRCS += TestSHA1.cpp
CPPSRCS += TestSegmentedVector.cpp
CPPSRCS += TestSplayTree.cpp
CPPSRCS += TestTypeTraits.cpp
CPPSRCS += TestTypedEnum.cpp
CPPSRCS += TestUniquePtr.cpp
CPPSRCS += TestVector.cpp
CPPSRCS += TestWeakPtr.cpp
DEFINES += -DIMPL_MFBT
DISABLE_STL_WRAPPING := 1
FAIL_ON_WARNINGS := 1
CPP_UNIT_TESTS += TestArrayUtils
STATIC_LIBS += $(DEPTH)/mfbt/libmfbt.a
CPP_UNIT_TESTS += TestAtomics
CPP_UNIT_TESTS += TestBinarySearch
CPP_UNIT_TESTS += TestBloomFilter
CPP_UNIT_TESTS += TestCasting
CPP_UNIT_TESTS += TestCeilingFloor
CPP_UNIT_TESTS += TestCheckedInt
CPP_UNIT_TESTS += TestCountPopulation
CPP_UNIT_TESTS += TestCountZeroes
CPP_UNIT_TESTS += TestEndian
CPP_UNIT_TESTS += TestEnumSet
CPP_UNIT_TESTS += TestFloatingPoint
CPP_UNIT_TESTS += TestIntegerPrintfMacros
CPP_UNIT_TESTS += TestJSONWriter
CPP_UNIT_TESTS += TestMacroArgs
CPP_UNIT_TESTS += TestMacroForEach
CPP_UNIT_TESTS += TestMaybe
CPP_UNIT_TESTS += TestPair
CPP_UNIT_TESTS += TestRefPtr
CPP_UNIT_TESTS += TestRollingMean
CPP_UNIT_TESTS += TestSegmentedVector
CPP_UNIT_TESTS += TestSHA1
CPP_UNIT_TESTS += TestSplayTree
CPP_UNIT_TESTS += TestTypedEnum
CPP_UNIT_TESTS += TestTypeTraits
CPP_UNIT_TESTS += TestUniquePtr
CPP_UNIT_TESTS += TestVector
CPP_UNIT_TESTS += TestWeakPtr
CPP_UNIT_TESTS += TestPoisonArea
