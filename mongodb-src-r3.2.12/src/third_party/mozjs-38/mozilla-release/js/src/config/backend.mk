# THIS FILE WAS AUTOMATICALLY GENERATED. DO NOT EDIT.

HOST_CSRCS += nsinstall.c
HOST_CSRCS += pathsub.c
NO_DIST_INSTALL := 1
PYTHON_UNIT_TESTS += tests/unit-expandlibs.py
PYTHON_UNIT_TESTS += tests/unit-mozunit.py
PYTHON_UNIT_TESTS += tests/unit-nsinstall.py
PYTHON_UNIT_TESTS += tests/unit-printprereleasesuffix.py
VISIBILITY_FLAGS := 
HOST_PROGRAM = nsinstall_real
